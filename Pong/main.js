/* ===========================================================================

  Pong by Robert Karpiński (2020)


  left paddle:  
    left shift key / left control key

  right paddle:  
    right shift key / right control key


=============================================================================*/

const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");

canvas.width = 1024;
canvas.height = (canvas.width * 3) / 4;

const screen_width = canvas.width;
const screen_height = canvas.height;

const ball_width = screen_width / 50;
const ball_V = ball_width * 20; // ball speed [px/s]
const ball_speed_after_bounce = 0.1;

const paddle_height = ball_width * 6;
const paddle_width = ball_width * 1.2;
const paddle_margin = ball_width * 2;

const paddle_V = ball_width * 15; // pad speed [px/s]
const paddle_max_speed_factor = 3;

const text_px = 5 * ball_width;

const line_count = 12;
const line_div_height = canvas.height / line_count;
const line_height = line_div_height * 0.6;
const line_height_margin = (line_div_height * 0.4) / 2;

let ball_speed_up = false;
let ball_start_x = 0;
let ball_start_y = screen_height / 2;
let last_timer = new Date().valueOf();
let ball_angle = 0;
let ball_add_v = 0;

let paddle_left_y = screen_height / 2 - paddle_height / 2;
let paddle_right_y = screen_height / 2 - paddle_height / 2;

let key_left_timer = new Date().valueOf(); // last change key
let key_left = 0; // 0 - nope 1 - paddle down,  -1 paddle up

let key_right_timer = new Date().valueOf(); // last change key
let key_right = 0; // 0 - nope 1 - paddle down,  -1 paddle up

let score_left = 0;
let score_right = 0;

// === keys =====

window.addEventListener("keydown", (event) => {
    switch (event.code) {
        case "ShiftLeft":
            if (key_left !== -1) {
                key_left = -1;
                key_left_timer = new Date().valueOf();
            }
            break;
        case "ShiftRight":
            if (key_right !== -1) {
                key_right = -1;
                key_right_timer = new Date().valueOf();
            }
            break;

        case "ControlLeft":
            if (key_left !== 1) {
                key_left = 1;
                key_left_timer = new Date().valueOf();
            }
            break;
        case "ControlRight":
            if (key_right !== 1) {
                key_right = 1;
                key_right_timer = new Date().valueOf();
            }
            break;
    }
});

window.addEventListener("keyup", (event) => {
    switch (event.code) {
        case "ShiftLeft":
            if (key_left === -1) key_left = 0;
            break;
        case "ShiftRight":
            if (key_right === -1) key_right = 0;
            break;

        case "ControlLeft":
            if (key_left === 1) key_left = 0;
            break;
        case "ControlRight":
            if (key_right === 1) key_right = 0;
            break;
    }
});

// == functions ====

function between(v, a, b) {
    if (v < a) return a;
    else if (v > b) return b;
    else return v;
}


function isTLeft(angle, border_x, ball_x) {
    if (angle >= 90 && angle <= 270) return ball_x < border_x;
    else return false;
}

function isTRight(angle, border_x, ball_x) {
    if (angle <= 90 || angle >= 270) return ball_x + ball_width > border_x;
    else return false;
}

function isTTop(angle, border_y, ball_y) {
    if (angle >= 180 && angle <= 360) return ball_y < border_y;
    else return false;
}

function isTBottom(angle, border_y, ball_y) {
    if (angle <= 180) return ball_y + ball_width > border_y;
    else return false;
}

function setBallAngle(angle) {
    ball_angle = angle;

    if (ball_angle < 0) ball_angle += 360;
    if (ball_angle >= 360) ball_angle -= 360;
}

function getPaddleSpeedFactor(timer) {
    const ms = new Date().valueOf() - timer;

    if (ms > 200) return paddle_max_speed_factor;
    else return (ms * (paddle_max_speed_factor - 1)) / 200 + 1;
}

function getPaddleBounceAngle(pad_y, ball_y) {
    const y = ((ball_y + ball_width / 2 - pad_y) * 2) / paddle_height - 1.0;
    return 45 * y;
}

function getBallSpeedFactor(angle) {
    return Math.abs(Math.sin(angle)) * 0.5 + 1 + ball_add_v;
}

function getBallSpeedInit() {
    return ((score_left + score_right) * ball_speed_after_bounce) / 10;
}

// === draw table ==

function table() {
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, screen_width, screen_height);
    ctx.lineWidth = ball_width / 3;

    ctx.strokeStyle = "#a0a0a0";

    ctx.beginPath();

    for (let i = 0; i < line_count; i++) {
        ctx.moveTo(screen_width / 2, i * line_div_height + line_height_margin);
        ctx.lineTo(
            screen_width / 2,
            i * line_div_height + line_height_margin + line_height
        );
    }

    ctx.stroke();

    ctx.fillStyle = "#a0a0a0";
    ctx.font = text_px + "px Arial";

    const a = ctx.measureText(score_left);

    ctx.fillText(
        score_left,
        screen_width / 2 - 2 * ball_width - a.width,
        text_px
    );
    ctx.fillText(score_right, screen_width / 2 + 2 * ball_width, text_px);

    const ms = new Date().valueOf();
    const delta = ((ms - last_timer) * paddle_V) / 1000;

    paddle_left_y += delta * key_left * getPaddleSpeedFactor(key_left_timer);
    paddle_right_y += delta * key_right * getPaddleSpeedFactor(key_right_timer);

    paddle_left_y = between(paddle_left_y, 0, screen_height - paddle_height);
    paddle_right_y = between(paddle_right_y, 0, screen_height - paddle_height);

    ctx.fillStyle = "#00FF00";
    ctx.fillRect(paddle_margin, paddle_left_y, paddle_width, paddle_height);

    ctx.fillStyle = "yellow";
    ctx.fillRect(
        screen_width - paddle_margin - paddle_width,
        paddle_right_y,
        paddle_width,
        paddle_height
    );
}

// === draw ball ==

function ball() {

    const ms = new Date().valueOf();
    const angle = (2 * Math.PI * ball_angle) / 360;
    const delta = ((ms - last_timer) * ball_V * getBallSpeedFactor(angle)) / 1000;

    const x = ball_start_x + Math.cos(angle) * delta;
    const y = ball_start_y + Math.sin(angle) * delta;

    if (ball_speed_up) ctx.fillStyle = "#888888";
    else ctx.fillStyle = "#222222";
    ctx.fillRect(ball_start_x, ball_start_y, ball_width, ball_width);

    if (ball_speed_up) ctx.fillStyle = "#ffeeee";
    else ctx.fillStyle = "white";

    ctx.fillRect(x, y, ball_width, ball_width);

    ball_start_x = x;
    ball_start_y = y;
    last_timer = ms;


    // -- calculate collisions ---

    if (isTLeft(ball_angle, 0, x)) {
        ball_start_x = screen_width + 20 * ball_width;
        score_right += 1;
        ball_add_v = getBallSpeedInit();
        ball_speed_up = false;
        return 1;
    }

    if (isTRight(ball_angle, screen_width, x)) {
        ball_start_x = -20 * ball_width;
        score_left += 1;
        ball_add_v = getBallSpeedInit();
        ball_speed_up = false;

        return 1;
    }

    if (isTTop(ball_angle, 0, y)) {
        ball_add_v -= ball_speed_after_bounce;

        setBallAngle(-ball_angle);
        return 2;
    }

    if (isTBottom(ball_angle, screen_height, y)) {
        ball_add_v -= ball_speed_after_bounce;

        setBallAngle(-ball_angle);
        return 2;
    }


    if (isTLeft(ball_angle, paddle_margin, x)) {
        return 0;
    }

    if (isTRight(ball_angle, screen_width - paddle_margin, x)) {
        return 0;
    }


    if (
        isTLeft(ball_angle, paddle_margin + paddle_width, x) &&
        isTBottom(180, paddle_left_y, y) &&
        isTTop(180, paddle_left_y + paddle_height, y)
    ) {
        ball_add_v += ball_speed_after_bounce;
        ball_speed_up = false;

        // speed up ball
        if (getPaddleSpeedFactor(key_left_timer) < paddle_max_speed_factor) {
            ball_add_v += ball_speed_after_bounce;
            ball_speed_up = true;
        }

        setBallAngle(getPaddleBounceAngle(paddle_left_y, y));
        return 3;
    }

    if (
        isTRight(ball_angle, screen_width - paddle_margin - paddle_width, x) &&
        isTBottom(180, paddle_right_y, y) &&
        isTTop(180, paddle_right_y + paddle_height, y)
    ) {
        ball_add_v += ball_speed_after_bounce;
        ball_speed_up = false;

        // speed up ball
        if (getPaddleSpeedFactor(key_right_timer) < paddle_max_speed_factor) {
            ball_add_v += ball_speed_after_bounce;
            ball_speed_up = true;
        }

        setBallAngle(180 - getPaddleBounceAngle(paddle_right_y, y));
        return 3;
    }

    ball_add_v = between(ball_add_v, getBallSpeedInit(), 5);

    return 0;
}


function play() {
    table();
    ball();

    window.requestAnimationFrame(play);
}

// === start ====

play();
